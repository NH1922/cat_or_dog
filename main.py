# -*- coding: utf-8 -*-
"""
Created on Mon Aug  6 00:27:00 2018

@author: NH
"""
import keras 
from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator
from scipy import ndimage
import time

start_time = time.time()
#initializing the cnn

classifier = Sequential()

#adding convolution layers 
#32 filter maps, 3x3 mask, input image (128,128) and 3 because of RGB for good accuracy
classifier.add(Convolution2D(filters=32,kernel_size=(3,3),
                             input_shape=(128,128,3),
                             activation ='relu'))
#pooling of 2,2
classifier.add(MaxPooling2D(pool_size=(2,2)))

#two mode convolution  layers and corresponding pooling 
classifier.add(Convolution2D(filters=32,kernel_size=(3,3),
                             activation ='relu'))
classifier.add(MaxPooling2D(pool_size=(2,2)))
classifier.add(Convolution2D(filters=32,kernel_size=(3,3),
                             activation ='relu'))
classifier.add(MaxPooling2D(pool_size=(2,2)))


#flatten
classifier.add(Flatten())

#making the remaining nn connections 
classifier.add(Dense(units = 128,activation='relu'))

#output layer 
classifier.add(Dense(units = 1,activation='sigmoid'))

#compiling the entire network
classifier.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])

#fitting to data 
train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory(r'E:\Anaconda\dataset\training_set',
                                                 target_size = (128, 128),
                                                 batch_size = 32,
                                                 class_mode = 'binary')

test_set = test_datagen.flow_from_directory(r'E:\Anaconda\dataset\test_set',
                                            target_size = (128, 128),
                                            batch_size = 32,
                                            class_mode = 'binary')


classifier.fit_generator(training_set,
                         steps_per_epoch = 8000,
                         epochs = 25,
                         validation_data = test_set,
                         validation_steps = 2000,workers=3)

classifier.save(filepath=r'E:\MyModel')

#saving as json data
# serialize model to JSON
classifer_json = classifier.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
classifier.save_weights("model.h5")
print("Saved model to disk")
time_taken = time.time()-start_time
print("TIme taken :",time_taken)
